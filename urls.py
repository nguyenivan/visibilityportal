from django.conf.urls.defaults import patterns, include, url
from django.views.generic.simple import direct_to_template
from django.conf import settings


# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

from djgrid.resources import GridResource
class Resource(GridResource):
    class Meta:
        register = [
				['auth','user'],
				['gridview','tradingpartner'],
				['gridview','agreement'],
				['gridview','documenttype'],
				['gridview','supplierdocument'],
				['gridview','buyerdocument'],
				['gridview','document'],
				['gridview','xreftable'],
				['gridview','xrefcode'],
				['gridview','viewcolumnmapping'],
				['gridview','xrefvalue'],
				['gridview','viewtablemapping'],
				]
        prefix = 'a'
resource = Resource()


urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'VisibilityPortal.views.home', name='home'),
    # url(r'^VisibilityPortal/', include('VisibilityPortal.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
	url(r'^accounts/', include('registration.backends.default.urls')),
    url(r'',include(resource.urls)),
    url(r'^$', direct_to_template, {'template': 'home.html'}),
)


if settings.DEBUG:
    urlpatterns += patterns('',
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
   )