# -*- coding: utf-8 -*-
from django.db import models
from django.utils.translation import ugettext_lazy as _

class TradingPartner(models.Model):
	name = models.CharField(_('Name'), max_length=50)
	def __unicode__ (self):
		return self.name
		
class Agreement(models.Model):
	buyer = models.ForeignKey(TradingPartner,verbose_name = _('Buyer'), related_name='buyer_agreements')
	supplier = models.ForeignKey(TradingPartner,verbose_name = _('Supplier'), related_name='supplier_agreements')
	def __unicode__ (self):
		return '%s buys from %s' % (self.buyer.name, self.supplier.name)

class DocumentType(models.Model):
	transaction_id = models.IntegerField(_('X12 Transaction ID'))
	name = models.CharField(_('Name'), max_length = 50)
	description = models.TextField(_('Description'))
	def __unicode__ (self):
		return '%s (%s)' % (self.name, self.transaction_id)

class SupplierDocument(models.Model):
	agreement = models.ForeignKey(Agreement, related_name='supplier_documents')
	document_type = models.ForeignKey(DocumentType, related_name ='supplier_agreements')
	
class BuyerDocument(models.Model):
	agreement = models.ForeignKey(Agreement, related_name='buyer_documents')
	document_type = models.ForeignKey(DocumentType, related_name ='buyer_agreements')
	
class Document(models.Model):
	document_type = models.ForeignKey(DocumentType, related_name ='documents')
	sender = models.ForeignKey(TradingPartner,verbose_name = _('Sender'), related_name='documents')
	content = models.TextField(_('Content'))

class XrefTable (models.Model):
	name =  models.CharField(_('Name'), max_length=50)
	agreement = models.ForeignKey(Agreement, related_name = 'xref_tables')
	notes = models.CharField(_('Notes'), max_length = 50, blank = True)
	def __unicode__ (self):
		return self.name
	
class XrefCode (models.Model):
	code = models.CharField(max_length=50)
	table = models.ForeignKey(XrefTable, verbose_name = _('Xref Table'), related_name = 'codes')
	default = models.CharField(_('Default Value'), max_length=50, blank=True)
	def __unicode__ (self):
		return self.code

class XrefValue(models.Model):
	name = models.CharField(_('Label'), max_length=50, blank=True)
	value = models.CharField(_('Value'), max_length=50)
	code = models.ForeignKey(XrefCode,verbose_name =  _('Code'), related_name = 'values')

class ViewTableMapping(models.Model):
	document_type = models.OneToOneField(DocumentType, blank=True, null = True)
	name = models.CharField(_('Name'), max_length=50)
	'this root_xpath is always relative. In case this table is top, the xpath is relative to /'
	root_xpath = models.CharField(_('Root XPath'), max_length=256)
	master_table = models.ForeignKey('self', related_name = 'detail_tables', blank=True, null=True)
	def __unicode__ (self):
		return self.name
	

class ViewColumnMapping(models.Model):
	name = models.CharField(_('Name'), max_length=50, blank=True)
	table = models.ForeignKey(ViewTableMapping, verbose_name = _('Table'))
	relative_xpath = models.CharField(_('Relative XPath to Root'), max_length=256)
	xref_name = models.CharField(_('Optional Xref Name'), max_length=50, blank=True)
	xref = models.ForeignKey(XrefTable,verbose_name = _('Xref Lookup'), blank= True, null=True)


	
